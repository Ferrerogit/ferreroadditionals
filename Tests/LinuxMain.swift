import XCTest

import FerreroAdditionalsTests

var tests = [XCTestCaseEntry]()
tests += FerreroAdditionalsTests.allTests()
XCTMain(tests)
