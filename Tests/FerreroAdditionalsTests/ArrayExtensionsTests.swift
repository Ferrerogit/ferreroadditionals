//
//  ArrayExtensionTests.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

import XCTest
@testable import FerreroAdditionals

private struct TestStruct: Equatable {
	let first: String
	let second: String
}

final class ArrayExtensionTests: XCTestCase {
    func testGroupBy() {
        let firstGroup = [
			TestStruct(first: "firstGroup", second: "1"),
			TestStruct(first: "firstGroup", second: "2"),
			TestStruct(first: "firstGroup", second: "3"),
		]
		
		let secondGroup = [
			TestStruct(first: "secondGroup", second: "1"),
			TestStruct(first: "secondGroup", second: "2"),
			TestStruct(first: "secondGroup", second: "3"),
		]
		
		let mixed = firstGroup + secondGroup
		
		let grouped = mixed.groupedBy(\.first)
				
		XCTAssertEqual(grouped["firstGroup"], firstGroup)
		XCTAssertEqual(grouped["secondGroup"], secondGroup)
    }

    static var allTests = [
        ("testGroupBy", testGroupBy),
    ]
}
