//
//  File.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

import UIKit

open class BaseView: UIView, Viewable {
	@available(*, unavailable)
	public required init?(coder: NSCoder) {
		super.init(coder: coder)
	}
	
	public init() {
		super.init(frame: .zero)
	}
}
