//
//  File.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

import UIKit

open class BaseVC<View: BaseView>: UIViewController, VCLifecycleHandling {

	var onViewDidLoad: ((View) -> Void)?
	
	var onViewWillAppear: ((View) -> Void)?
	var onViewDidAppear: ((View) -> Void)?
	
	var onViewWillDisappear: ((View) -> Void)?
	var onViewDidDisappear: ((View) -> Void)?
	
	override open func loadView() {
		self.view = BaseView()
	}
	
	open override func viewDidLoad() {
		super.viewDidLoad()

		self.onViewDidLoad?(self.view as! View)
	}
	
	open override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		self.onViewWillAppear?(self.view as! View)
	}
	
	open override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		self.onViewDidAppear?(self.view as! View)
	}
	
	open override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		self.onViewWillDisappear?(self.view as! View)
	}
	
	open override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		
		self.onViewDidDisappear?(self.view as! View)
	}
}
