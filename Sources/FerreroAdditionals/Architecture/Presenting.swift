//
//  Presenting.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

public protocol Presenting: AnyObject {
	associatedtype SubPresenter: SubPresenting
}

public protocol SubPresenting {}

extension Never: SubPresenting {}
