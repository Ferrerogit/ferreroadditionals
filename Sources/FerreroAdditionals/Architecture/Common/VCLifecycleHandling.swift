//
//  File.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

protocol VCLifecycleHandling: AnyObject {
	associatedtype View: BaseView
	
	var onViewDidLoad: ((View) -> Void)? { get set }
	var onViewWillAppear: ((View) -> Void)? { get set }
	var onViewDidAppear: ((View) -> Void)? { get set }
	var onViewWillDisappear: ((View) -> Void)? { get set }
	var onViewDidDisappear: ((View) -> Void)? { get set }
}
