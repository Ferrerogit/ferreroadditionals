//
//  FPair.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

public struct FPair<First, Second> {
	public let first: First
	public let second: Second
	
	public init(first: First, second: Second) {
		
		self.first = first
		self.second = second
	}
}
