//
//  Array.swift
//  
//
//  Created by Ярослав Попов on 02.01.2020.
//

public extension Array {
	func paired<T>(with anotherArray: [T]) -> [FPair<Element, T>] {
		return self.flatMap { firstElement in
			anotherArray.map { secondElement in
				FPair(first: firstElement, second: secondElement)
			}
		}
	}
	
	func groupedBy<Key: Hashable>(_ keyPath: KeyPath<Element, Key>) -> [Key: [Element]] {
		return Dictionary(grouping: self, by: { $0[keyPath: keyPath] } )
	}
	
	func uniqueBy<Key: Hashable>(_ keyPath: KeyPath<Element, Key>) -> [Element] {
		var keysContainer = Set<Key>()
		return self.reduce(into: [Element]()) { (res, arg) in
			let key = arg[keyPath: keyPath]
			guard keysContainer.insert(key).inserted == false else { return }
			
			res.append(arg)
		}
	}

	func uniqueAndPrefBy<Key: Hashable>(_ keyPath: KeyPath<Element, Key>,
								 prefExpression: (Element, Element) throws -> Bool) rethrows -> [Element] {
		let values = try self
			.groupedBy(keyPath)
			.compactMapValues { try $0.sorted(by: prefExpression).last }
			.values
		
		return Array(values)
	}
}
